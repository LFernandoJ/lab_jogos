import pygame
from ball import Ball
# Inicia e cria Janela
pygame.init()

display = pygame.display.set_mode([800, 600])

pygame.display.set_caption("Pong")

def draw():
    display.fill([150,20,200])

#Objects
objectGroup = pygame.sprite.Group()
ball = Ball(objectGroup)
ball.rect.x = 75
gameLoop = True

sentido = "Direita"

speed = 0.1

if __name__ == "__main__":
    while gameLoop:


        #Fechar jogo
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameLoop = False


        if (ball.rect.x == 750):
            sentido = "Esquerda"
        if (ball.rect.x == 1):
            sentido = "Direita"
        if ball.rect.x < 751 and sentido == "Direita":
            ball.moveRight()
        elif ball.rect.x > 0 and sentido == "Esquerda":
            ball.moveLeft()
            
        
        display.fill([50,50,50])
        objectGroup.draw(display)
        pygame.display.update()